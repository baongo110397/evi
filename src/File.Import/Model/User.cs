﻿namespace File.Import.Model
{
    public class User 
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        /// 
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string Dect_CD { get; set; }

        /// <summary>
        /// Gets or sets the company address.
        /// </summary>
        /// <value>
        /// The company address.
        /// </value>
        public string CompanyAddress { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>
        /// The company.
        /// </value>
        public string User_CD { get; set; }
    }
}